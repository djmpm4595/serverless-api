const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const uuid = require('node-uuid');

const request = require('request');

const { PERSONAS_TABLE, IS_OFFLINE } = process.env;

const dynamoDb =
  IS_OFFLINE === 'true'
    ? new AWS.DynamoDB.DocumentClient({
        region: 'localhost',
        endpoint: 'http://localhost:8000',
      })
    : new AWS.DynamoDB.DocumentClient();

app.use(bodyParser.json({ strict: false }));

// INI RELLENAR DATA
rellenar_data();
function rellenar_data(){
  const params = {
    TableName: PERSONAS_TABLE,
  }

  dynamoDb.scan(params, (error, result) => {
    if (error) {
      console.log("Error...")
    }
    if(result.Items == null || result.Items.length == 0){
      var count = 0;

      request('https://swapi.py4e.com/api/people', { json: true }, (err, rpta, body) => {
        if (err) { console.log(err); }
        count = parseInt(body.count);
        guardar_bd(body.results);
        if(count > 10){
          var num_pages = Math.ceil(count / 10);
          for(var i = 1; i < num_pages; i++){
            request('https://swapi.py4e.com/api/people/?page='+(i+1), { json: true }, (err2, rpta2, body2) => {
              if (err2) { console.log(err2); }
              guardar_bd(body2.results);
            });
          }
        }
      });
    }
  });
}

function guardar_bd(results){
  for(var i = 0; i < results.length; i++){
    var result = results[i];
    const personaId = uuid.v4();
    const nombre              = result.name;
    const altura              = result.height;
    const peso                = result.mass;
    const color_cabello       = result.hair_color;
    const color_piel          = result.skin_color;
    const color_ojos          = result.eye_color;
    const anio_nacimiento     = result.birth_year;
    const sexo                = result.gender;
    const fecha_creacion      = result.created;
    const fecha_actualizacion = result.edited;
    const link                = result.url;
    const params = {
      TableName: PERSONAS_TABLE,
      Item: {
        personaId,
        nombre,
        altura,
        peso,
        color_cabello,
        color_piel,
        color_ojos,
        anio_nacimiento,
        sexo,
        fecha_creacion,
        fecha_actualizacion,
        link
      },
    }
    dynamoDb.put(params, error => {
      if (error) {
        console.log('Error creando persona: ', error)
      }
    });
  }
}
// FIN RELLENAR DATA

// INI LISTAR
app.get('/personas', (req, res) => {
  const params = {
    TableName: PERSONAS_TABLE,
  }

  dynamoDb.scan(params, (error, result) => {
    if (error) {
      res.status(400).json({ error: 'Error retrieving Peoples' })
    }
    const { Items: personas } = result;
    res.json({ personas });
  });
});
// FIN LISTAR

// INI REGISTRAR
app.post('/personas', (req, res) => {
  const personaId = uuid.v4();
  const nombre = req.body.name;
  const altura = req.body.height;
  const peso = req.body.mass;
  const color_cabello = req.body.hair_color;
  const color_piel = req.body.skin_color;
  const color_ojos = req.body.eye_color;
  const anio_nacimiento = req.body.birth_year;
  const sexo = req.body.gender;
  const fecha_creacion = req.body.created;
  const fecha_actualizacion = req.body.edited;
  const link = req.body.url;

  const params = {
    TableName: PERSONAS_TABLE,
    Item: {
      personaId,
      nombre,
      altura,
      peso,
      color_cabello,
      color_piel,
      color_ojos,
      anio_nacimiento,
      sexo,
      fecha_creacion,
      fecha_actualizacion,
      link
    },
  }

  dynamoDb.put(params, error => {
    if (error) {
      console.log('Error creando Persona: ', error)
      res.status(400).json({ error: 'Could not create Persona' })
    }
    res.json({
      personaId,
      nombre,
      altura,
      peso,
      color_cabello,
      color_piel,
      color_ojos,
      anio_nacimiento,
      sexo,
      fecha_creacion,
      fecha_actualizacion,
      link
    })
  })
});
// FIN REGISTRAR

// INI CONSULTAR REGISTRO
app.get('/personas/:personaId', (req, res) => {
  const { personaId } = req.params

  const params = {
    TableName: PERSONAS_TABLE,
    Key: {
      personaId,
    },
  }

  dynamoDb.get(params, (error, result) => {
    if (error) {
      res.status(400).json({ error: 'Error obteniendo Persona' })
    }

    if (result.Item) {
      const {
        personaId,
        nombre,
        altura,
        peso,
        color_cabello,
        color_piel,
        color_ojos,
        anio_nacimiento,
        sexo,
        fecha_creacion,
        fecha_actualizacion,
        link
      } = result.Item
      res.json({
        personaId,
        nombre,
        altura,
        peso,
        color_cabello,
        color_piel,
        color_ojos,
        anio_nacimiento,
        sexo,
        fecha_creacion,
        fecha_actualizacion,
        link
      })
    } else {
      res.status(404).json({ error: `Persona con id: ${personaId} no encontrada` })
    }
  })
});
// FIN CONSULTAR REGISTRO

// INI EDITAR REGISTRO
app.put('/personas', (req, res) => {
  const {
    personaId,
    nombre,
    altura,
    peso,
    color_cabello,
    color_piel,
    color_ojos,
    anio_nacimiento,
    sexo,
    fecha_creacion,
    fecha_actualizacion,
    link
  } = req.body

  var params = {
    TableName: PERSONAS_TABLE,
    Key: { personaId },
    UpdateExpression: 'set #a = :nombre, #b = :altura, #c = :peso, #d = :color_cabello, #e = :color_piel, #f = :color_ojos, #g = :anio_nacimiento, #h = :sexo, #i = :fecha_creacion, #j = :fecha_actualizacion, #k = :link',
    ExpressionAttributeNames: { '#a': 'nombre', '#b': 'altura', '#c': 'peso', '#d': 'color_cabello', '#e': 'color_piel', '#f': 'color_ojos', '#g': 'anio_nacimiento', '#h': 'sexo', '#i': 'fecha_creacion', '#j': 'fecha_actualizacion', '#k': 'link' },
    ExpressionAttributeValues: { ':nombre': nombre, ':altura': altura, ':peso': peso, ':color_cabello': color_cabello, ':color_piel': color_piel, ':color_ojos': color_ojos, ':anio_nacimiento': anio_nacimiento, ':sexo': sexo, ':fecha_creacion': fecha_creacion, ':fecha_actualizacion': fecha_actualizacion, ':link': link },
  }

  dynamoDb.update(params, error => {
    if (error) {
      console.log(`Error actualizando Persona con id ${personaId}: `, error)
      res.status(400).json({ error: 'No se pudo actualizar a la persona' })
    }

    res.json({
      personaId,
      nombre,
      altura,
      peso,
      color_cabello,
      color_piel,
      color_ojos,
      anio_nacimiento,
      sexo,
      fecha_creacion,
      fecha_actualizacion,
      link
    })
  })
});
// FIN EDITAR REGISTRO

// INI ELIMINAR REGISTRO
app.delete('/personas/:personaId', (req, res) => {
  const { personaId } = req.params

  const params = {
    TableName: PERSONAS_TABLE,
    Key: {
      personaId,
    },
  }

  dynamoDb.delete(params, error => {
    if (error) {
      console.log(`Error eliminando Persona con id ${personaId}`, error)
      res.status(400).json({ error: 'No se pudo eliminar a la persona' })
    }

    res.json({ success: true })
  })
});
// FIN ELIMINAR REGISTRO

module.exports.handler = serverless(app);